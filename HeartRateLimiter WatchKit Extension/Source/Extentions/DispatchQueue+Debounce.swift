//
//  DispatchQueue+Debounce.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 17.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation

extension DispatchQueue {
    static func debounce(waitSeconds: TimeInterval, completion: @escaping () -> Void) {
        let backgroundQueue = DispatchQueue.global()
        let deadline = DispatchTime.now() + waitSeconds
        backgroundQueue.asyncAfter(deadline: deadline, qos: .background) {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}
