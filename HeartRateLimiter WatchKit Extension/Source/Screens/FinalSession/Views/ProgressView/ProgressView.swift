//
//  ProgressView.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arkhipov on 03.05.2021.
//  Copyright © 2021 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct ProgressText: View {
    var isLoading = false
    var text = ""
    
    var body: some View {
        VStack {
            Text(self.text)
                .font(.system(size: 27, design: .rounded))
                .opacity(self.isLoading ? 0 : 1)
                .animation(
                    self.isLoading ? Animation
                        .easeIn(duration: 1.0)
                        .repeatForever(autoreverses: true) : .default
                )
        }
    }
}

#if DEBUG

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ProgressText(isLoading: false, text: "000")
        }
    }
}

#endif
