//
//  SelectableItems.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 14.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import HealthKit

let activityTypes = [
    SelectableItem<HKWorkoutActivityType>(title: "Функциональная", value: .crossTraining),
    SelectableItem<HKWorkoutActivityType>(title: "Смешанная кардио", value: .mixedCardio),
    SelectableItem<HKWorkoutActivityType>(title: "Бег", value: .running),
    SelectableItem<HKWorkoutActivityType>(title: "Ходьба", value: .walking)
]

let locationTypes = [
    SelectableItem<HKWorkoutSessionLocationType>(title: "В помещении", value: .indoor),
    SelectableItem<HKWorkoutSessionLocationType>(title: "На улице", value: .outdoor)
]
