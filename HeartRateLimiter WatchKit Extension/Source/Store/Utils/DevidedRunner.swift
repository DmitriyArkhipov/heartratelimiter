//
//  DevidedRunner.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Дмитрий Архипов on 04.08.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import DateToolsSwift

final class DevidedRunner {
    static let shared = DevidedRunner()
    
    private var lastRun: Date?
    private var defaultRecoverySec = 90 // 1m 30sec - значение получено опытным путем
                                        // это среднее по больнице время восстановление
                                        // сердцебеения при интенсивных нагрузках
    
    func run(_ block: @escaping () -> Void) {
        let runBlockAndSaveDate = {
            block()
            
            self.lastRun = Date()
        }
        
        if let lastRun = self.lastRun, lastRun.secondsAgo >= self.defaultRecoverySec {
            runBlockAndSaveDate()
        } else if self.lastRun == nil {
            runBlockAndSaveDate()
        }
    }
}
