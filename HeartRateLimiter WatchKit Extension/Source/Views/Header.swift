//
//  Header.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 13.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct Header: View {
    let title: String
    
    var body: some View {
        HStack {
            Text(self.title)
                .font(.system(size: 14, design: .rounded))
                .foregroundColor(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: 0.5))
            Spacer()
        }
    }
}

#if DEBUG

struct Header_Previews: PreviewProvider {
    static var previews: some View {
        Header(title: "Заголовок")
    }
}

#endif
