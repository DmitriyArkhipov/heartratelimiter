//
//  HRLTimer.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 10.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import Combine

enum HRLTimerType {
    case milliseconds
    case seconds100
}

class HRLTimer {
    private var cancellable: AnyCancellable?
    private var timer: Publishers.Autoconnect<Timer.TimerPublisher>?
    private var seconds: Double = 0
    
    private let receiveTime: (String) -> Void
    private let defaultRepeatTimeInterval: Double = 0.05
    private let type: HRLTimerType
    
    init(type: HRLTimerType? = .seconds100, onReceiveTime: @escaping (String) -> Void) {
        self.receiveTime = onReceiveTime
        self.type = type!
    }
    
    func start() {
        self.timer = Timer.publish (every: self.defaultRepeatTimeInterval, on: .current, in: .common).autoconnect()
        
        self.subscribeOnTimer()
    }
    
    func stop() {
        self.timer?.upstream.connect().cancel()
        
        self.unsubscribeOnTimer()
    }
    
    func pause() {
        self.stop()
    }
    
    func unpause() {
        self.start()
    }
    
    private func subscribeOnTimer() {
        self.cancellable = self.timer?.sink(receiveValue: {[weak self] value in
            switch self?.type {
            case .milliseconds:
                self?.changeTimeWithMilliseconds()
            case .seconds100:
                self?.changeTimeWithSeconds100()
            default:
                break
            }
        })
    }
    
    private func unsubscribeOnTimer() {
        self.cancellable = nil
    }
    
    private func changeTimeWithMilliseconds() {
        self.seconds += self.defaultRepeatTimeInterval
        
        let minutes = Int((self.seconds / 60).truncatingRemainder(dividingBy: 60))
        let seconds = Int((self.seconds).truncatingRemainder(dividingBy: 60))
        let milliseconds = Int((self.seconds * 1000).truncatingRemainder(dividingBy: 1000))
        
        let formattedMinutes = self.tenthsableString(from: minutes)
        let formattedSeconds = self.tenthsableString(from: seconds)
        let formattedMilliseconds = self.hundredableString(from: milliseconds)
        
        let time = "\(formattedMinutes):" + "\(formattedSeconds)," + "\(formattedMilliseconds)"
        
        self.receiveTime(time)
    }
    
    private func changeTimeWithSeconds100() {
        self.seconds += self.defaultRepeatTimeInterval
        
        let minutes = Int((self.seconds / 60).truncatingRemainder(dividingBy: 60))
        let seconds = Int((self.seconds).truncatingRemainder(dividingBy: 60))
        let seconds100 = Int((self.seconds * 100).truncatingRemainder(dividingBy: 100))
        
        let formattedMinutes = self.tenthsableString(from: minutes)
        let formattedSeconds = self.tenthsableString(from: seconds)
        let formattedSeconds100 = self.tenthsableString(from: seconds100)
        
        let time = "\(formattedMinutes):" + "\(formattedSeconds)," + "\(formattedSeconds100)"
        
        self.receiveTime(time)
    }
    
    private func tenthsableString(from value: Int) -> String {
        if value < 10 {
            return "0\(value)"
        }
        
        return "\(value)"
    }
    
    private func hundredableString(from value: Int) -> String {
        if value < 10 {
            return "00\(value)"
        }
        
        if value < 100 {
            return "0\(value)"
        }
        
        return "\(value)"
    }
}
