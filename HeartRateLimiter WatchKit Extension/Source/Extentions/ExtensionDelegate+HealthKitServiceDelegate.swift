//
//  ExtensionDelegate+HealthKitServiceDelegate.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 23.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import HealthKit

extension ExtensionDelegate: HealthKitServiceDelegate {
    func didChange(error: Error) {
        AppStore.dispatch(AppAction.change(error: error))
    }
    
    func didChange(rate: Double) {
        AppStore.dispatch(AppAction.change(rate: rate))
    }
    
    func didChange(state: HKWorkoutSessionState) {
        AppStore.dispatch(AppAction.change(workoutState: state))
    }
}
