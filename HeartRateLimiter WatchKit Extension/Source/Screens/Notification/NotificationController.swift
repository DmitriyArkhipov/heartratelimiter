//
//  NotificationController.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 23.04.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import WatchKit
import SwiftUI
import UserNotifications

class NotificationController: WKUserNotificationHostingController<NotificationView> {
    
    private var heartRate: Int = 0
    private var message: String = ""

    override var body: NotificationView {
        return NotificationView(
            heartRate: self.heartRate,
            message: self.message
        )
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    override func didReceive(_ notification: UNNotification) {
        let userInfo = notification.request.content.userInfo
        
        guard
            let heartRate = userInfo["heart_rate"] as? Double,
            let message = userInfo["message"] as? String
        else {
            return
        }
        
        self.heartRate = Int(heartRate)
        self.message = message
    }
}
