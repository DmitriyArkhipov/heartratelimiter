//
//  SmallAnimatedButton.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 16.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct SmallAnimatedButton: View {
    let imageName: String
    let onTap: () -> Void
    
    @State private var animatedScale: CGFloat = 1
    
    var body: some View {
        Image(imageName).onTapGesture {
            self.onTap()
            
            WKInterfaceDevice.current().play(.click)
            
            withAnimation(.easeIn(duration: 0.1), {
                self.animatedScale = 0.8
            })
            withAnimation(.easeOut(duration: 0.2), {
                self.animatedScale = 1.0
            })
        }
        .frame(width: 26, height: 26)
        .background(Color.heartRed)
        .cornerRadius(/*@START_MENU_TOKEN@*/26.0/*@END_MENU_TOKEN@*/)
        .scaleEffect(animatedScale)
    }
}

#if DEBUG

struct SmallAnimatedButton_Previews: PreviewProvider {
    static var previews: some View {
        SmallAnimatedButton(imageName: "Plus", onTap: {
            print("test")
        })
    }
}

#endif
