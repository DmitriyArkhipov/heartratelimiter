//
//  FinalSessionView.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 14.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct FinalSessionView: View {
    @ObservedObject var viewModel = FinalSessionViewModel()
    
    var onClose: () -> Void
    var onDisappearCompletion: () -> Void
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Header(title: "Средний пульс")
                HStack{
                    ProgressText(isLoading: self.viewModel.fetchData, text: self.viewModel.averageHeartRate)
                    Text("уд/мин")
                        .font(.system(size: 27, design: .rounded))
                        .foregroundColor(Color.heartRed)
                }
                Header(title: "Время тренеровки")
                    .padding(.top, 1.0)
                Text(self.viewModel.finalTime)
                    .font(.system(size: 27, design: .rounded))
                Group {
                    if (!self.viewModel.location.isEmpty && !self.viewModel.activityType.isEmpty) {
                        Header(title: "Тип тренеровки")
                        Text(self.viewModel.activityType)
                            .font(.system(size: 18, design: .rounded))
                        Text(self.viewModel.location)
                            .font(.system(size: 18, design: .rounded))
                    }
                }
                Header(title: "Данные о тренеровке можно посмотреть в приложении Активность или Здоровье")
                    .padding(.top, 1.0)
                Button("Закрыть", action: {
                    self.viewModel.completeSession()
                    self.onClose()
                })
                .padding(.top, 10.0)
                .frame(maxWidth: .infinity)
            }
            .frame(maxWidth: .infinity)
        }
        .navigationBarTitle("Статистика")
        .navigationBarBackButtonHidden(true)
        .onAppear {
            self.viewModel.subscripeOnStore()
        }
        .onDisappear {
            self.viewModel.unsubscribeOnStore()
            self.onDisappearCompletion()
        }
    }
}

#if DEBUG

struct FinalSession_Previews: PreviewProvider {
    static var previews: some View {
        FinalSessionView(
            onClose: {
                print("onClose")
            },
            onDisappearCompletion: {}
        )
    }
}

#endif
