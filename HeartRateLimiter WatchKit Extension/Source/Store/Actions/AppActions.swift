//
//  AppActions.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 19.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk
import HealthKit

enum AppActionType: Action {
    case start
    case stop
    case pause
    case resume
    case complete
    
    case changeError(Error)
    case changeRate(Double)
    case changeFinalTime(String)
    case changeWorkoutState(HKWorkoutSessionState)
    
    case fetchFinalData
    case fetchedFinalData(HKWorkout, Double)
    
    case setActivityType(HKWorkoutActivityType)
    case setLocationType(HKWorkoutSessionLocationType)
    case setReteLimit(Int)
    
    case resetSession
}

final class AppAction {
    static func set(activityType: HKWorkoutActivityType) -> Thunk<AppState> {
       return Thunk<AppState> { dispatch, _ in
            HealthKitService.shared.set(activityType: activityType)
        
            dispatch(AppActionType.setActivityType(activityType))
       }
    }
    
    static func set(locationType: HKWorkoutSessionLocationType) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            HealthKitService.shared.set(locationType: locationType)

            dispatch(AppActionType.setLocationType(locationType))
        }
    }
    
    static func resetSession() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            dispatch(AppActionType.resetSession)
        }
    }
    
    
    static func setRateLimit(_ rateLimit: Double) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            dispatch(AppActionType.setReteLimit(Int(rateLimit)))
        }
    }
    
    static func increaseRateLimit() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard var heartRateLimit = getState()?.heartRateLimit else {
                return
            }
            
            heartRateLimit += 1
            
            dispatch(AppActionType.setReteLimit(Int(heartRateLimit)))
        }
    }
    
    static func decreaseRateLimit() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard var heartRateLimit = getState()?.heartRateLimit else {
                return
            }
            
            if heartRateLimit == 0 {
                return
            }

            heartRateLimit -= 1

            dispatch(AppActionType.setReteLimit(Int(heartRateLimit)))
        }
    }
    
    static func start() -> Thunk<AppState> {
       return Thunk<AppState> { dispatch, _ in
            HealthKitService.shared.startActivity(
                succeed: {
                    dispatch(AppActionType.start)
                },
                failure: { error in
                    dispatch(AppActionType.changeError(error))
                }
            )
       }
    }
    
    static func stop() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            dispatch(AppActionType.fetchFinalData)
            
            HealthKitService.shared.stopActivity { hwWorkout in
                HealthKitService.shared.getAverageHeartLimit(duration: hwWorkout.duration, succeed: { averageHeartRate in
                    dispatch(AppActionType.fetchedFinalData(hwWorkout, averageHeartRate))
                }, failure: { error in
                    dispatch(AppActionType.changeError(error))
                })
            }
            
            dispatch(AppActionType.stop)
        }
    }
    
    static func pause() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            HealthKitService.shared.pauseActivity()
            
            dispatch(AppActionType.pause)
        }
    }
    
    static func resume() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            HealthKitService.shared.resumeActivity()
            
            dispatch(AppActionType.resume)
        }
    }
    
    static func complete() -> Thunk<AppState> {
           return Thunk<AppState> { dispatch, _ in
               dispatch(AppActionType.complete)
           }
       }
    
    static func change(error: Error) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            dispatch(AppActionType.changeError(error))
        }
    }
    
    static func change(rate: Double) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            dispatch(AppActionType.changeRate(rate))
        }
    }
    
    static func change(workoutState: HKWorkoutSessionState) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            dispatch(AppActionType.changeWorkoutState(workoutState))
        }
    }
    
    static func change(finalTime: String) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, _ in
            dispatch(AppActionType.changeFinalTime(finalTime))
        }
    }
}
