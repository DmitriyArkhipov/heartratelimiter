//
//  SessionViewModel.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 09.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import Combine
import ReSwift
import ReSwiftThunk

class SessionViewModel: ObservableObject {
    @Published var currentHeartRate = 0
    @Published var heartRateLimit = 0
    @Published var lastHeartRate = 0
    @Published var time = "00:00,00"
    @Published var trainingType: String = ""
    @Published var lastMeasurementStatus: String = "пока не зафиксирован"
    @Published var sessionButtonsVisible: Bool = false
    @Published var sessionPlayButtonState: StateblePlayButtonState = .pause
    
    var sessionCompleted = false
    
    private let serialActionsQueue = DispatchQueue(label: "SessionViewModel_Serial_Sync")
    
    // Тамером владеет модель
    // в целях оптимизации перерендера компонентов
    // обновлять все переменные не нужно, просто при обновлении таймера
    private lazy var timer = {
        return HRLTimer() { [weak self] time in
            DispatchQueue.main.async {
                self?.time = time
            }
        }
    }()

    func startSession() {
        if self.sessionCompleted {
            return
        }
        
        self.timer.start()
        
        serialActionsQueue.async {
            AppStore.dispatch(AppAction.start())
        }
    }
    
    func pauseSession() {
        self.timer.pause()
        
        serialActionsQueue.async {
            AppStore.dispatch(AppAction.pause())
        }
    }
    
    func resumeSession() {
        self.timer.start()
        
        serialActionsQueue.async {
            AppStore.dispatch(AppAction.resume())
        }
    }
    
    func stopSession(completion: @escaping () -> Void) {
        self.timer.stop()
        
        serialActionsQueue.async {
            AppStore.dispatch(AppAction.stop())
            AppStore.dispatch(AppAction.change(finalTime: self.time))
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func toggleSessionState() {
        switch self.sessionPlayButtonState {
        case .play:
            self.resumeSession()
        case .pause:
            self.pauseSession()
        }
    }
}

extension SessionViewModel: StoreSubscriber {
    func subscripeOnStore() {
        AppStore.subscribe(self) { subscription in
            subscription.select(SessionViewState.init)
        }
    }

    func unsubscribeOnStore() {
        AppStore.unsubscribe(self)
    }

    func newState(state: SessionViewState) {
        DispatchQueue.main.async {
            self.heartRateLimit = state.heartRateLimit
            self.currentHeartRate = state.currentHeartRate
            self.lastHeartRate = state.lastHeartRate
            self.sessionPlayButtonState = state.sessionPlayButtonState
            self.lastMeasurementStatus = state.lastMeasurementStatus
            self.trainingType = state.trainingType
            
            // bugfix HealthKit при старте отдает значения 0
            // по всем параметрам и если начать управлять сессией
            // она может зависнуть
            if self.currentHeartRate != 0 {
                self.sessionButtonsVisible = true
            }
        }
    }
}
