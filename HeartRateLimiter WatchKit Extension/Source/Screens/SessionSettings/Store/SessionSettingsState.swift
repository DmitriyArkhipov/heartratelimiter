//
//  SessionSettingsState.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 08.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation

struct SessionSettingsState: Equatable {
    static func == (lhs: SessionSettingsState, rhs: SessionSettingsState) -> Bool {
        return lhs.rateLimit == rhs.rateLimit && lhs.sessionState == rhs.sessionState
    }
    
    let rateLimit: Int
    let sessionState: SessionState?
    
    init(state: AppState) {
        self.rateLimit = Int(state.heartRateLimit)
        self.sessionState = state.sessionState
    }
}
