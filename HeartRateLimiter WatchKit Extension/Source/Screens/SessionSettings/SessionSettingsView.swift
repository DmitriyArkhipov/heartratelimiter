//
//  SessionSettingsView.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 14.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI
import HealthKit


struct SessionSettingsView: View {
    @ObservedObject var viewModel = SessionSettingsViewModel()
    
    @State private var selectorActivityPresented: Bool = false
    @State private var locationSelectorPresented = false
    @State private var inputHeartRatePresented = false
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView (.vertical) {
                VStack(alignment: .center) {
                    Text("Максимальный пульс")
                    .font(.headline)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .padding(.top, 2.0)
                    .padding(.horizontal, 5.0)
                    
                    HStack {
                        Spacer()
                        SmallAnimatedButton(
                            imageName: "Plus",
                            onTap: {
                                self.viewModel.increaseRateLimit()
                            }
                        )
                        Spacer()
                        Text("\(Int(self.viewModel.rateLimit))")
                            .font(.system(size: 30, design: .rounded))
                            .padding(.vertical, 5.0)
                            .padding(.horizontal, 16.0)
                            .overlay(
                                RoundedRectangle(cornerRadius: 8)
                                    .stroke(Color.heartRed, lineWidth: 1.5)
                            )
                            .onTapGesture {
                                self.inputHeartRatePresented = true
                            }
                        Spacer()
                        SmallAnimatedButton(
                            imageName: "Minus",
                            onTap: {
                                self.viewModel.decreaseRateLimit()
                            }
                        )
                        Spacer()
                    }
                    .padding(.top, 2.0)
                    
                    Text("Тип тренеровки")
                        .font(.headline)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.leading)
                        .padding(.top, 2.0)
                    
                    SelectorButton(
                        title: self.viewModel.selectionActivityType?.title ?? "Выбрать",
                        onTap: {
                            self.selectorActivityPresented = true
                        }
                    )
                    .padding(.top)
                    .padding(.horizontal, 2.0)
                    
                    SelectorButton(
                        title: self.viewModel.selectionLocationType?.title ?? "Выбрать",
                        onTap: {
                            self.locationSelectorPresented = true
                        }
                    )
                    .padding(.top)
                    .padding(.horizontal, 2.0)
                    
                    ActionButton(title: "Старт", onTap: {
                        WKInterfaceDevice.current().play(.start)
                        
                        if self.viewModel.selectionActivityType == nil {
                            self.selectorActivityPresented = true
                            
                            return
                        }

                        self.viewModel.start()
                    })
                    .padding(.top, 30.0)
                    .padding(.horizontal, 2.0)

                    NavigationLink(
                        "",
                        destination: SessionView(onClose: {
                            self.viewModel.startSessionPresended = false
                        }),
                        isActive: self.$viewModel.startSessionPresended
                    )
                    .accentColor(Color.clear)
                    .frame(width: 0, height: 0)
                    .hidden()
                    
                    NavigationLink(
                        "",
                        destination: SelectionListView(
                            title: "Выбрать тип тренеровки",
                            selectableItems: activityTypes,
                            showingModal: self.$selectorActivityPresented,
                            changedSelection: self.$viewModel.selectionActivityType
                        ),
                        isActive: self.$selectorActivityPresented
                    )
                    .accentColor(Color.clear)
                    .frame(width: 0, height: 0)
                    .hidden()
                    
                    NavigationLink(
                        "",
                        destination: SelectionListView(
                            title: "Выбрать место для тренеровки",
                            selectableItems: locationTypes,
                            showingModal: self.$locationSelectorPresented,
                            changedSelection: self.$viewModel.selectionLocationType
                        ),
                        isActive: self.$locationSelectorPresented
                    )
                    .accentColor(Color.clear)
                    .frame(width: 0, height: 0)
                    .hidden()
                    
                    NavigationLink(
                        "",
                        destination: HeartRateInput(
                            changeHeartRateLimit: self.$viewModel.rateLimit,
                            heartRateLimit: self.viewModel.rateLimit
                        ){
                            self.inputHeartRatePresented = false
                        },
                        isActive: self.$inputHeartRatePresented
                    )
                    .accentColor(Color.clear)
                    .frame(width: 0, height: 0)
                    .hidden()
                }
            }
            .navigationBarTitle("Настройка")
            .frame(
                width: geometry.size.width,
                height: geometry.size.height,
                alignment: .leading
            )
            .onAppear {
                self.viewModel.subscripeOnStore()
            }
        }
    }
}

#if DEBUG

struct SessionSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SessionSettingsView()
    }
}

#endif
