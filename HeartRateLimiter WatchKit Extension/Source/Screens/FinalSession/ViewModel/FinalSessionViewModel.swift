//
//  FinalSessionViewModel.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arkhipov on 12.07.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import ReSwift
import Combine

class FinalSessionViewModel: ObservableObject {
    @Published var finalTime: String = "00:00:00"
    @Published var averageHeartRate: String = "000"
    @Published var fetchData: Bool = true
    @Published var activityType: String = ""
    @Published var location: String = ""
    
    func completeSession() {
        AppStore.dispatch(AppAction.complete())
        AppStore.dispatch(AppAction.resetSession())
    }
}

extension FinalSessionViewModel: StoreSubscriber {
    func subscripeOnStore() {
        AppStore.subscribe(self)
    }

    func unsubscribeOnStore() {
        AppStore.unsubscribe(self)
    }
    
    func newState(state: AppState) {
        DispatchQueue.main.async {
            self.finalTime = state.finalTime ?? "00:00,00"
            self.averageHeartRate = state.averageHeartRate != 0 ? "\(Int(state.averageHeartRate))" : "000"
            self.fetchData = state.fetchFinalData
            self.activityType = activityTypes.first(where: { $0.value == state.activityType })?.title ?? ""
            self.location = locationTypes.first(where: { $0.value == state.locationType })?.title ?? ""
        }
    }
}
