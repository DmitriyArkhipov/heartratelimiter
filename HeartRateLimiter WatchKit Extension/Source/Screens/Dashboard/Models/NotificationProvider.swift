//
//  NotificationProvider.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 26.04.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import UserNotifications

final class NotificationProvider {
    static func submitNotification(
        title: String,
        userInfo: [AnyHashable : Any],
        completion: (([Error]) -> Void)? = nil
    ) {
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.01, repeats: false)
        
        let notificationContent = UNMutableNotificationContent()
        
        notificationContent.title = title
        notificationContent.userInfo = userInfo
        notificationContent.sound = UNNotificationSound.default
        notificationContent.categoryIdentifier = "AlarmHeartLimit"
        
        let request = UNNotificationRequest(
            identifier: UUID().uuidString,
            content: notificationContent,
            trigger: trigger
        )
        
        submitNotifications(with: [request], completion: completion)
    }
    
    static func submitNotifications(
        with requests: [UNNotificationRequest],
        completion: (([Error]) -> Void)? = nil
    ) {
        let dispatchGroup = DispatchGroup()
        let writeQueue = DispatchQueue(label: "unnotificationRegistrator.serial.writequeue")

        var operationErrors = [Error]()

        for request in requests {
                let center = UNUserNotificationCenter.current()

                dispatchGroup.enter()

                center.add(request, withCompletionHandler: { error in
                    dispatchGroup.leave()
                    
                    guard let error = error else {
                        return
                    }

                    writeQueue.async {
                        operationErrors.append(error)
                        
                        dispatchGroup.leave()
                    }
                })
        }

        dispatchGroup.notify(queue: DispatchQueue.main) {
            completion?(operationErrors)
        }
    }
}
