//
//  HeartRateInput.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arkhipov on 07.05.2021.
//  Copyright © 2021 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct HeartRateInput: View {
    @Binding var changeHeartRateLimit: Double
    @State var heartRateLimit: Double = 150.0
    @State private var xOffset: CGFloat = 0.0
    
    var onClose: () -> Void
    
    private var activeAnimation: Animation {
        if self.xOffset != 0.0 {
            return Animation.easeInOut(duration: 0.7)
                .repeatForever(autoreverses: true)
        } else {
            return Animation.default
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Image("CrownHelpArrow")
                    .offset(x: self.xOffset, y: 8)
                    .animation(self.activeAnimation)
                    .onAppear {
                        DispatchQueue.debounce(waitSeconds: 0.5, completion: {
                            self.xOffset = -4.0
                        })
                    }
            }
            Spacer()
            Text("\(Int(self.heartRateLimit))")
                .font(.system(size: 40, design: .rounded))
                .padding(.vertical, 5.0)
                .padding(.horizontal, 16.0)
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(Color.heartRed, lineWidth: 1.5)
                )
                .focusable(true)
                .digitalCrownRotation(
                    self.$heartRateLimit,
                    from: 0,
                    through: 200,
                    by: 1,
                    sensitivity: .low,
                    isContinuous: false,
                    isHapticFeedbackEnabled: true
                )
            Spacer()
            Button("Готово") {
                self.changeHeartRateLimit = heartRateLimit
                self.onClose()
            }
            
        }
        .navigationTitle("Макс. пульс")
    }
}

#if DEBUG

struct HeartRateInput_PreviewView: View {
    @State private var test: Double = 20.0
    
    var body: some View {
        HeartRateInput(changeHeartRateLimit: self.$test) {}
    }
}

struct HeartRateInput_Previews: PreviewProvider {
    static var previews: some View {
        HeartRateInput_PreviewView()
    }
}

#endif
