//
//  SessionSettingsViewModel.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 14.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import Combine
import HealthKit
import ReSwift
import ReSwiftThunk

class SessionSettingsViewModel: ObservableObject {
    @Published var rateLimit: Double = 0 {
        didSet {
            AppStore.dispatch(AppAction.setRateLimit(rateLimit))
        }
    }
    @Published var selectionActivityType: SelectableItem<HKWorkoutActivityType>?
    @Published var selectionLocationType: SelectableItem<HKWorkoutSessionLocationType>?
    @Published var startSessionPresended = false
    
    init() {
        self.selectionLocationType = locationTypes.first
    }
    
    func increaseRateLimit() {
        AppStore.dispatch(AppAction.increaseRateLimit())
    }
    
    func decreaseRateLimit() {
        AppStore.dispatch(AppAction.decreaseRateLimit())
    }
    
    func start() {
        guard let selectionActivityType = self.selectionActivityType else {
            return
        }
        
        AppStore.dispatch(AppAction.set(activityType: selectionActivityType.value))
        AppStore.dispatch(AppAction.set(locationType: self.selectionLocationType!.value))
        
        self.startSessionPresended = true
    
        self.unsubscribeOnStore()
    }
}

extension SessionSettingsViewModel: StoreSubscriber {
    func subscripeOnStore() {
        AppStore.subscribe(self) { subscription in
           subscription.select(SessionSettingsState.init)
       }
    }
    
    func unsubscribeOnStore() {
        AppStore.unsubscribe(self)
    }

    func newState(state: SessionSettingsState) {
        self.rateLimit = Double(state.rateLimit)
        
        if let sessionState = state.sessionState, sessionState == .completed {
            self.startSessionPresended = false
        }
    }
}
