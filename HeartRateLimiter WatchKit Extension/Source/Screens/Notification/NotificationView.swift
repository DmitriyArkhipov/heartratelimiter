//
//  NotificationView.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 23.04.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    let heartRate: Int
    let message: String
    
    @State var heartAnimated = false
    @State var heartBigAnimated = false
    @State var heartSmallAnimated = false
    
    private var animationBigWithRate: (Int) -> Animation {
        return { rate in
            var duration = 0.1
            
            if rate <= 70 {
                duration = 0.7
            } else if rate <= 150 {
                duration = 0.5
            } else {
                duration = 0.3
            }
            
            return Animation
                .easeInOut(duration: duration)
                .repeatForever(autoreverses: false)
        }
    }
    
    private var animationSmallWithRate: (Int) -> Animation {
        return { rate in
            var duration = 0.1
            
            if rate <= 70 {
                duration = 0.7
            } else if rate <= 150 {
                duration = 0.5
            } else {
                duration = 0.3
            }
            
            return Animation
                .easeIn(duration: duration)
                .repeatForever(autoreverses: false)
        }
    }
    
    private var animationWithRate: (Int) -> Animation {
        return { rate in
            var duration = 0.1
            
            if rate <= 70 {
                duration = 0.7
            } else if rate <= 150 {
                duration = 0.5
            } else {
                duration = 0.3
            }
            
            return Animation
                .easeIn(duration: duration)
                .repeatForever(autoreverses: false)
        }
    }
    
    var body: some View {
        VStack {
            ZStack {
                Image("HeartBorderBig")
                    .scaleEffect(self.heartAnimated ? 1.2 : 0)
                    .animation(
                        self.heartAnimated ? self.animationBigWithRate(self.heartRate) : nil
                    )
                    .frame(width: 153.0, height: 133.0)
                Image("HeartBorderSmall")
                    .scaleEffect(self.heartAnimated ? 1.2 : 0)
                    .animation(
                        self.heartAnimated ? self.animationSmallWithRate(self.heartRate) : nil
                    )
                    .frame(width: 135.0, height: 117.0)
                Image("BigHeart")
                    .frame(width: 117.0, height: 102.0)
                    .scaleEffect(self.heartAnimated ? 1.2 : 1)
                    .animation(
                        self.heartAnimated ? self.animationWithRate(self.heartRate) : nil
                    )

                Text("\(self.heartRate)")
                    .font(.system(size: 38, design: .rounded))
                    .multilineTextAlignment(.center)
            }
            Text(self.message)
                .font(.system(size: 17, design: .rounded))
                .multilineTextAlignment(.center)
                .foregroundColor(Color(red: 1.0, green: 1.0, blue: 1.0, opacity: 0.5))
                .padding([.top, .leading, .trailing])
                .fixedSize(horizontal: false, vertical: true)
        }
        .onAppear {
            DispatchQueue.debounce(waitSeconds: 0.5, completion: {
                self.heartAnimated = true
            })
        }
        .onDisappear {
            self.heartAnimated = false
        }
    }
}

#if DEBUG

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView(heartRate: 55, message: "Пульс выше ограничения \n170 уд/мин")
    }
}

#endif
