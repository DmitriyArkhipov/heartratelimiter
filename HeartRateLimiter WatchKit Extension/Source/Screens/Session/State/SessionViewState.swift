//
//  SessionViewState.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arkhipov on 28.09.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation

struct SessionViewState: Equatable {
    static func == (lhs: SessionViewState, rhs: SessionViewState) -> Bool {
        return lhs.currentHeartRate == rhs.currentHeartRate
            && lhs.heartRateLimit == rhs.heartRateLimit
            && lhs.lastHeartRate == rhs.lastHeartRate
            && lhs.time == rhs.time
            && lhs.trainingType == rhs.trainingType
            && lhs.lastMeasurementStatus == rhs.lastMeasurementStatus
            && lhs.sessionPlayButtonState == rhs.sessionPlayButtonState
    }
    
    var currentHeartRate = 0
    var heartRateLimit = 0
    var lastHeartRate = 0
    var time = "00:00:00"
    var trainingType: String = ""
    var lastMeasurementStatus: String = "пока не зафиксирован"
    var sessionPlayButtonState: StateblePlayButtonState = .pause
    
    init(state: AppState) {
        self.heartRateLimit = Int(state.heartRateLimit)
        self.currentHeartRate = Int(state.currentHeartRate)
        self.lastHeartRate = Int(state.lastMaxHeartRate?.rate ?? 0)
        self.trainingType = activityTypes.first(where: { $0.value == state.activityType })?.title ?? ""

        guard let lastMaxHeartRate = state.lastMaxHeartRate else {
            return
        }
        
        let diffTime = lastMaxHeartRate.date.time(to: Date())
        
        self.lastMeasurementStatus = "\(diffTime) назад"
        
        guard let sessionState = state.sessionState else {
            return
        }
        
        switch sessionState {
        case .started, .resumed:
            self.sessionPlayButtonState = .pause
        case .paused:
            self.sessionPlayButtonState = .play
        case .stoped, .completed:
            self.sessionPlayButtonState = .play
        }
    }

}
