//
//  Date+TimeWithUnits.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 14.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation

extension Date {
    func time(to date: Date) -> String {
        let dateComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: self, to: date)
        let hour = dateComponents.hour!
        let minutes = dateComponents.minute!
        let seconds = dateComponents.second!
        
        if hour == 0 && minutes == 0 {
            return "\(seconds) сек."
        }
        
        let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"

        if hour == 0 {
            return "\(minutes) мин."
        }

        return "\(hour) ч. \(minutesString) мин."
    }
}
