//
//  ActionButton.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 19.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct ActionButton: View {
    let title: String
    let onTap: () -> Void
    
    @State private var animatedScale: CGFloat = 1
    
    var body: some View {
        Button(title) {
            self.onTap()
        }
        .buttonStyle(BorderedButtonStyle(tint: Color.heartRed.opacity(255)))
        .foregroundColor(Color.black)
        .fontWeight(.bold)
    }
}

#if DEBUG

struct ActionButton_Previews: PreviewProvider {
    static var previews: some View {
        ActionButton(title: "Test", onTap: {})
    }
}

#endif
