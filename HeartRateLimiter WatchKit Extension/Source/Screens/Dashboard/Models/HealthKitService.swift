//
//  HealthKitService.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 19.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import HealthKit

protocol HealthKitServiceDelegate: AnyObject {
    func didChange(error: Error)
    func didChange(rate: Double)
    func didChange(state: HKWorkoutSessionState)
}

final class HealthKitService: NSObject {
    static let shared = HealthKitService()
    
    weak var delegate: HealthKitServiceDelegate?
    
    private var workoutSession: HKWorkoutSession?
    private var workoutBuilder: HKLiveWorkoutBuilder?
    private let healthStore = HKHealthStore()
    private let configuration = HKWorkoutConfiguration()
    
    // The quantity type to write to the health store.
    private let typesToShare: Set = [
        HKQuantityType.workoutType()
    ]
    
    // The quantity types to read from the health store.
    private let typesToRead: Set = [
        HKQuantityType.quantityType(forIdentifier: .heartRate)!,
        HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!,
        HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
    ]
    
    override init() {
        super.init()
        
        // Request authorization for those quantity types.
        healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in
            // Handle errors here.
            guard success else {
                self.delegate?.didChange(error: error!)
                
                return
            }
        }
        
        let authorizationStatus = healthStore.authorizationStatus(for: HKWorkoutType.workoutType())
        
        if authorizationStatus != .sharingAuthorized {
            // app is not authorized to write workout to health store
            return
        }
    }
    
    // MARK: - Build Config
    
    func set(activityType: HKWorkoutActivityType) {
        self.configuration.activityType = activityType
    }
    
    func set(locationType: HKWorkoutSessionLocationType) {
        self.configuration.locationType = locationType
    }
    
    // MARK: - Manage Activity
    
    func startActivity(
        succeed: @escaping () -> Void,
        failure: @escaping (Error) -> Void)
    {
        guard let workoutSession = try? HKWorkoutSession(
            healthStore: healthStore,
            configuration: self.configuration
        ) else {
            return
        }
        
        let workoutBuilder = workoutSession.associatedWorkoutBuilder()
        
        workoutBuilder.dataSource = HKLiveWorkoutDataSource(
            healthStore: healthStore,
            workoutConfiguration: configuration
        )
        
        // reset delegates
        workoutBuilder.delegate = self
        workoutSession.delegate = self
        
        self.workoutSession = workoutSession
        self.workoutBuilder = workoutBuilder
        
        // start new session, becouse last session cant be restarted
        self.workoutSession?.startActivity(with: Date())
        
        self.workoutBuilder?.beginCollection(withStart: Date()) { (success, error) in
            guard success else {
                if let unwrapedError = error {
                    failure(unwrapedError)
                }

                return
            }

            succeed()
        }
    }
    
    func stopActivity(completion: ((HKWorkout) -> Void)? = nil) {
        self.workoutSession?.end()
        self.workoutSession?.stopActivity(with: Date())
        
        self.workoutBuilder?.endCollection(withEnd: Date()) { (success, error) in
            guard success else {
                return
            }
            
            self.workoutBuilder?.finishWorkout { (workout, error) in
                if let error = error {
                    self.delegate?.didChange(error: error)
                }
                
                guard let workout = workout else {
                    return
                }
                
                // clear resources for restart
                self.workoutSession?.delegate = nil
                self.workoutBuilder?.delegate = nil
                
                self.workoutSession = nil
                self.workoutBuilder = nil
                
                completion?(workout)
            }
        }
    }
    
    func pauseActivity() {
        self.workoutSession?.pause()
    }
    
    func resumeActivity() {
        self.workoutSession?.resume()
    }
    
    deinit {
        self.stopActivity()
    }
    
    func getAverageHeartLimit(duration: TimeInterval, succeed: @escaping (Double) -> Void, failure: ((Error) -> Void)? = nil) {
        let calendar = Calendar.current

        let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!

        // Start days back, end with today
        let endDate = Date()
        let startDate = { () -> Date in
            var currentDate = Date()
            
            currentDate.addTimeInterval(-duration)
            
            return currentDate
        }()

        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

        // Set the anchor to exactly midnight
        let anchorDate = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!

        // Generate daily statistics
        var interval = DateComponents()
        interval.day = 1

        // Create the query
        let query = HKStatisticsCollectionQuery(
            quantityType: heartRateType,
            quantitySamplePredicate: predicate,
            options: .discreteAverage,
            anchorDate: anchorDate,
            intervalComponents: interval
        )

        // Set the results handler
        query.initialResultsHandler = { query, results, error in
            if let error = error {
                failure?(error)
                
                return
            }
            
            guard let statsCollection = results else { return }

            for statistics in statsCollection.statistics() {
                guard let quantity = statistics.averageQuantity() else { continue }

                let beatsPerMinuteUnit = HKUnit.count().unitDivided(by: HKUnit.minute())
                let value = quantity.doubleValue(for: beatsPerMinuteUnit)
                
                succeed(value)
            }
        }

        HKHealthStore().execute(query)
    }
}

extension HealthKitService: HKWorkoutSessionDelegate {
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        self.delegate?.didChange(state: toState)
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        self.delegate?.didChange(error: error)
    }
}

extension HealthKitService: HKLiveWorkoutBuilderDelegate {
    func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) {}
    
    func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
        let state = workoutBuilder.statistics(for: HKQuantityType.quantityType(forIdentifier: .heartRate)!)?.averageQuantity()
        
        let heartRateUnit = HKUnit(from: "count/min")
        let currentHeartRate = state?.doubleValue(for: heartRateUnit) ?? 0
        
        self.delegate?.didChange(rate: currentHeartRate)
    }
}
