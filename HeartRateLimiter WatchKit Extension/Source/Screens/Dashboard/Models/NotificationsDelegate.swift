//
//  NotificationsDelegate.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 26.04.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import UserNotifications
//import WatchKit

class NotificationsDelegate: NSObject {
    static let shared = NotificationsDelegate()
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
                guard granted else { return }
                
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                  guard settings.authorizationStatus == .authorized else { return }
//                    WKExtension.shared().registerForRemoteNotifications()
                }
            }
    }
}

extension NotificationsDelegate: UNUserNotificationCenterDelegate {
    public func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler([.alert, .sound, .badge])
    }

    public func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        completionHandler()
    }
}
