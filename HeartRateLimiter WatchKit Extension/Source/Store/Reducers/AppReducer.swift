//
//  AppReducer.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 19.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import ReSwift

final class AppReducer {
        static func reduce(
        action: Action,
        state: AppState?
    ) -> AppState {
            var mutableState = state!
            
            guard let appAction = action as? AppActionType else {
                return mutableState
            }
            
            switch appAction {
            case .start:
                mutableState.sessionState = .started
                mutableState.fetchFinalData = false
            case .pause:
                mutableState.sessionState = .paused
            case .resume:
                mutableState.sessionState = .resumed
            case .complete:
                mutableState.sessionState = .completed
            case .stop:
                mutableState.sessionState = .stoped
            case .setReteLimit(let rateLimit):
                mutableState.heartRateLimit = Double(rateLimit)
            case .changeRate(let rate):
                let roundedRate = rate.rounded(.toNearestOrAwayFromZero)
                
                if let lastRate = mutableState.lastMaxHeartRate, roundedRate > lastRate.rate {
                    mutableState.lastMaxHeartRate = LastMaxHeartRate(rate: roundedRate)
                } else if mutableState.lastMaxHeartRate == nil && roundedRate > mutableState.currentHeartRate {
                    mutableState.lastMaxHeartRate = LastMaxHeartRate(rate: roundedRate)
                }
                
                mutableState.currentHeartRate = roundedRate
            case .changeWorkoutState(let workoutState):                
                mutableState.workoutSessionState = workoutState
            case .changeFinalTime(let time):
                mutableState.finalTime = time
            case .setActivityType(let activityType):
                mutableState.activityType = activityType
            case .setLocationType(let locationType):
                mutableState.locationType = locationType
            case .resetSession:
                mutableState.lastMaxHeartRate = nil
                mutableState.currentHeartRate = 0
                
            case .fetchFinalData:
                mutableState.fetchFinalData = true
            case .fetchedFinalData(_, let averageHeartRate):
                mutableState.averageHeartRate = averageHeartRate.rounded(.toNearestOrAwayFromZero)
                mutableState.fetchFinalData = false
            default:
                break
            }
            
            return mutableState
    }
}
