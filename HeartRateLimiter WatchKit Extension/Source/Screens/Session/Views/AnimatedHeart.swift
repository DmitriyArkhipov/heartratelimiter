//
//  AnimatedHeart.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 13.06.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct AnimatedHeart: View {
    let rate: Int
    
    @State var heartAnimated = false
    
    private var animationWithRate: (Int) -> Animation {
        return { rate in
            var duration = 0.1
            
            if rate <= 70 {
                duration = 0.5
            } else if rate <= 150 {
                duration = 0.4
            } else {
                duration = 0.28
            }
            
            return Animation
                .easeIn(duration: duration)
                .repeatForever(autoreverses: false)
        }
    }
    
    var body: some View {
            Image("SmallHeart")
                .scaleEffect(self.heartAnimated ? 1.2 : 1)
                .animation(
                    self.heartAnimated ? self.animationWithRate(self.rate) : nil
                )
                .onAppear {
                    DispatchQueue.debounce(waitSeconds: 0.5, completion: {
                        self.heartAnimated = true
                    })
                }
                .onDisappear {
                    self.heartAnimated = false
                }
    }
}

#if DEBUG

struct AnimatedHeart_Previews: PreviewProvider {
    static var previews: some View {
        AnimatedHeart(rate: 200)
    }
}

#endif
