//
//  StateblePlayButton.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arkhipov on 02.07.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

enum StateblePlayButtonState: String {
    case play = "Play"
    case pause = "Pause"
}

struct StateblePlayButton: View {
    let state: StateblePlayButtonState
    let action: () -> Void
    
    var body: some View {
        Button(action: self.action) {
            Image(self.state.rawValue)
        }
        .buttonStyle(BorderedButtonStyle(tint: colorFromState(state: self.state).opacity(255)))
    }
    
    private func colorFromState(state: StateblePlayButtonState) -> Color {
        switch state {
        case .play:
            return .green
        case .pause:
            return .yellow
        }
    }
}

#if DEBUG

struct StateblePlayButton_Previews: PreviewProvider {
    static var previews: some View {
        StateblePlayButton(state: .play, action: {})
    }
}

#endif
