//
//  SelectionListView.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 16.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI
import HealthKit

struct SelectableItem<T>: Identifiable {
    let id = UUID().uuidString
    
    let title: String
    let value: T
}

struct SelectedItemRow<T>: View {
    let onTap: () -> Void
    let item: SelectableItem<T>
    
    @Binding var selection: SelectableItem<T>?
    
    var body: some View {
        Button(item.title, action: {
            self.selection = self.item
            
            self.onTap()
        })
        .overlay(
            RoundedRectangle(cornerRadius: 40)
                .stroke(
                    self.selectedColor(),
                    lineWidth: 3
                )
        )
        .padding(.vertical, 2)
    }
    
    
    private func selectedColor() -> Color {
        guard
            let selectedItem = self.selection,
            selectedItem.id == self.item.id
        else {
            return Color.clear
        }
        
        return Color.heartRed
    }
}


struct SelectionListView<T>: View {
    let title: String
    let selectableItems: [SelectableItem<T>]
    
    @Binding var showingModal: Bool
    @Binding var changedSelection: SelectableItem<T>?
    
    @State private var selection: SelectableItem<T>?
    
    var body: some View {
        ScrollView {
            VStack {
                Text(self.title)
                    .font(.headline)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .padding(.top, 5.0)
                Spacer()
                ForEach(self.selectableItems) { item in
                    SelectedItemRow(
                        onTap: {
                            self.changedSelection = self.selection
                            
                            WKInterfaceDevice.current().play(.click)
                            
                            DispatchQueue.debounce(
                                waitSeconds: 0.05,
                                completion: {
                                    self.showingModal = false
                                }
                            )
                        },
                        item: item,
                        selection: self.$selection
                    )
                    Spacer()
                }
            }
            .padding(.horizontal, 3.0)
            .onAppear {
                DispatchQueue.main.async {
                    self.selection = self.changedSelection
                }
            }
        }
    }
}

#if DEBUG

struct SelectionListView_PreviewView: View {
    @State private var showingModal = true
    @State private var selection: SelectableItem<HKWorkoutActivityType>?
    
    let activityTypes =  [
        SelectableItem<HKWorkoutActivityType>(title: "Функциональная", value: .crossTraining),
        SelectableItem<HKWorkoutActivityType>(title: "Смешанная кардио", value: .mixedCardio),
        SelectableItem<HKWorkoutActivityType>(title: "Бег", value: .running),
        SelectableItem<HKWorkoutActivityType>(title: "Ходьба", value: .walking)
    ]
    
    var body: some View {
        SelectionListView(
            title: "Выбрать тип тренеровки",
            selectableItems: activityTypes,
            showingModal: $showingModal,
            changedSelection: $selection
        )
    }
}

struct SelectionListView_Previews: PreviewProvider {
    static var previews: some View {
        SelectionListView_PreviewView()
    }
}

#endif
