//
//  AppMiddleware.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 29.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk

let appMiddleware: Middleware<AppState> = { dispatch, getState in
    return { next in
        return { action in
            guard let appAction = action as? AppActionType else {
                next(action)
                
                return
            }
            
            switch appAction {
            case .changeRate(let rate):
                guard
                    let rateLimit = getState()?.heartRateLimit
                else {
                    break
                }
                
                if rate >= rateLimit {
                    DevidedRunner.shared.run {
                        NotificationProvider.submitNotification(
                            title: "Снижайте темп!",
                            userInfo: [
                                "heart_rate": rate,
                                "message": "Пульс выше ограничения \n\(Int(rateLimit)) уд/мин"
                            ]
                        )
                    }
                }
            default:
                break
            }
            
            next(action)
        }
    }
}
