//
//  Color+Hex.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 15.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

extension Color {
    static var heartRed: Color {
        return Color.init(UIColor.init(hex: "#F7444F")!)
    }
    
    static var heartRedDisabled: Color {
        return Color.init(UIColor.init(hex: "#F7444F")!.withAlphaComponent(0.8))
    }
    
    static var textDisabled: Color {
        return Color.init(UIColor.white.withAlphaComponent(0.8))
    }
}
