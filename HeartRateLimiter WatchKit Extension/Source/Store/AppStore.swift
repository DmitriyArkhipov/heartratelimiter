//
//  AppStore.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 19.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk

var AppStore: Store<AppState> = {
    let thunksMiddleware: Middleware<AppState> = createThunkMiddleware()

    return Store<AppState>(
        reducer: AppReducer.reduce,
        state: AppState(),
        middleware: [
            thunksMiddleware,
            appMiddleware
        ]
    )
}()
