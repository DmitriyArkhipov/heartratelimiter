//
//  AppState.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 13.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import Foundation
import HealthKit
import ReSwift

struct LastMaxHeartRate: Identifiable {
    let id: String
    
    let rate: Double
    let date: Date

    init(rate: Double) {
        self.rate = rate
        self.date = Date()
        self.id = UUID().uuidString
    }
}

enum SessionState {
    case paused
    case started
    case resumed
    case stoped
    case completed
}

struct AppState: StateType {
    var finalTime: String?
    var currentHeartRate: Double = 0
    var heartRateLimit: Double = 150
    var lastMaxHeartRate: LastMaxHeartRate?
    var activityType: HKWorkoutActivityType?
    var locationType: HKWorkoutSessionLocationType?
    var authorizationStatus: HKAuthorizationStatus?
    var workoutSessionState: HKWorkoutSessionState?
    var error: Error?
    var sessionState: SessionState?
    
    var fetchFinalData: Bool = false
    var averageHeartRate: Double = 0
}
