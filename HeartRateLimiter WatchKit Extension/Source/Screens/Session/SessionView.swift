//
//  SessionView.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 14.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

//fileprivate var sessionCompleted = false

struct SessionView: View {
    @ObservedObject var viewModel = SessionViewModel()
    
    @State var showFinalSessionStat = false
    
    var onClose: () -> Void
    
    var body: some View {
            ScrollView (.vertical) {
                VStack() {
                    HStack {
                        Text(self.viewModel.trainingType)
                            .font(.system(size: 18, design: .rounded))
                        Spacer()
                    }
                    Header(title: "Время")
                        .padding(.top, 3.0)
                    HStack {
                        Text(self.viewModel.time)
                            .font(.system(size: 34, design: .rounded))
                        Spacer()
                    }
                    Header(title: "Текущий пульс")
                    HStack {
                        Text("\(self.viewModel.currentHeartRate)")
                            .frame(width: 60.0, alignment: .leading)
                            .font(.system(size: 36, design: .rounded))
                        VStack(alignment: .leading) {
                            Text("УД/МИН")
                                .font(.system(size: 14, design: .rounded))
                                .foregroundColor(Color.heartRed)
                            AnimatedHeart(rate: self.viewModel.currentHeartRate)
                                .padding(.top, 3.0)
                        }
                        Spacer()
                    }
                    Header(title: "Последний максимум")
                    Header(title: self.viewModel.lastMeasurementStatus)
                    HStack {
                        Text("\(self.viewModel.lastHeartRate)")
                            .frame(width: 60.0, alignment: .leading)
                            .font(.system(size: 36, design: .rounded))
                            .foregroundColor(Color.textDisabled)
                        VStack(alignment: .leading) {
                            Text("УД/МИН")
                                .font(.system(size: 14, design: .rounded))
                                .foregroundColor(Color.heartRedDisabled)
                        }
                        Spacer()
                    }
                    Header(title: "Ограничение пульса")
                    HStack {
                        Text("\(self.viewModel.heartRateLimit)")
                            .frame(width: 60.0, alignment: .leading)
                            .font(.system(size: 36, design: .rounded))
                            .foregroundColor(Color.textDisabled)
                        VStack(alignment: .leading) {
                            Text("УД/МИН")
                                .font(.system(size: 14, design: .rounded))
                                .foregroundColor(Color.heartRedDisabled)
                        }
                        Spacer()
                    }
                }
                HStack {
                    Group {
                        if (self.viewModel.sessionButtonsVisible) {
                            StateblePlayButton(
                                state: self.viewModel.sessionPlayButtonState,
                                action: {
                                    self.viewModel.toggleSessionState()
                                }
                            )
                            Button(action: {
                                self.viewModel.stopSession(completion: {
                                    showFinalSessionStat = true
                                })
                            }) {
                                Image("Exit")
                            }
                            .buttonStyle(BorderedButtonStyle(tint: .red.opacity(255)))
                            .sheet(isPresented: $showFinalSessionStat, content: {
                                    FinalSessionView(
                                        onClose: {
                                            self.showFinalSessionStat = false
                                            self.viewModel.sessionCompleted = true
                                            
                                        },
                                        onDisappearCompletion: {
                                            self.onClose()
                                        }
                                    )
                                }
                            )
                        }
                    }
                }
            }
            .navigationBarTitle("Тренеровка")
            .navigationBarBackButtonHidden(true)
            .onAppear {
                if self.viewModel.sessionCompleted || self.showFinalSessionStat {
                    return
                }

                self.viewModel.subscripeOnStore()
                
                self.viewModel.startSession()
            }
            .onDisappear {
                self.viewModel.unsubscribeOnStore()
            }
    }
}

#if DEBUG

struct SessionView_Previews: PreviewProvider {
    static var previews: some View {
        SessionView(onClose: {
            print("onClose")
        })
    }
}

#endif
