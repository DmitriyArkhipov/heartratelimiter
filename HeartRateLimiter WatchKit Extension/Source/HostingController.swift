//
//  HostingController.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 23.04.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<SessionSettingsView> {
    override var body: SessionSettingsView {
        return SessionSettingsView()
    }
}
