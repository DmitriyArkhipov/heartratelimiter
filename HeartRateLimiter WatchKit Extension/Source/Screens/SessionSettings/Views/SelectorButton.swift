//
//  SelectorButton.swift
//  HeartRateLimiter WatchKit Extension
//
//  Created by Dmitriy Arhipov on 17.05.2020.
//  Copyright © 2020 Dmitriy Arhipov. All rights reserved.
//

import SwiftUI

struct SelectorButton: View {
    let title: String
    let onTap: () -> Void
    
    @State private var animatedScale: CGFloat = 1
    
    var body: some View {
        Button(title, action: onTap)
    }
}

#if DEBUG

struct SelectorButton_Previews: PreviewProvider {
    static var previews: some View {
        SelectorButton(title: "Test test stes tststst",onTap: {
            print("onTap")
        })
    }
}

#endif
